﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject
{
    public class Person
    {
        public string FirstName { get; set; }        
        public string SecondName { get; set; }
        public int Age { get; set; }
        public DateTime BirthDay { get; set; }
        public bool IsFamily { get; set; }
    }
}
