﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject
{
    public class Book
    {
        public string Title { get; set; }
        public int CountPages { get; set; }
    }
}
